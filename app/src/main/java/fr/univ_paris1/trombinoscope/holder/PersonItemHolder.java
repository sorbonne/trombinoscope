package fr.univ_paris1.trombinoscope.holder;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by salahddine on 26/02/2017.
 */

public class PersonItemHolder {
    public TextView   firstName;
    public TextView lastName;
    public  ImageView avatar;
}
