package fr.univ_paris1.trombinoscope.dao;

import java.util.List;

import fr.univ_paris1.trombinoscope.dto.Person;

/**
 * Created by salahddine on 26/02/2017.
 */

public interface IPersonDAO {

    public List<Person> getPersons();

    public void addPerson(Person person);

    public void removePerson(Person person);
}
