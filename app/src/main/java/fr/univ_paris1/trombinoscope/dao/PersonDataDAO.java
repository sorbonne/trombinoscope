package fr.univ_paris1.trombinoscope.dao;

import java.util.List;

import fr.univ_paris1.trombinoscope.dao.IPersonDAO;
import fr.univ_paris1.trombinoscope.data.DataPerson;
import fr.univ_paris1.trombinoscope.dto.Person;

/**
 * Created by salahddine on 26/02/2017.
 */

public class PersonDataDAO implements IPersonDAO {
    @Override
    public List<Person> getPersons() {
        return  DataPerson.persons;
    }

    @Override
    public void addPerson(Person person) {
        DataPerson.persons.add(person);
    }

    @Override
    public void removePerson(Person person) {DataPerson.persons.remove(person);}
}
