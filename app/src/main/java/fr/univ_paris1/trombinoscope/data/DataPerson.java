package fr.univ_paris1.trombinoscope.data;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

import fr.univ_paris1.trombinoscope.dto.Person;

/**
 * Created by salahddine on 26/02/2017.
 */
public class DataPerson {
    public static List<Person> persons =new ArrayList<Person>() {{
        add(new Person("Salahddine", "ABERKAN", Color.BLACK));
        add(new Person("Salahddine1", "ABERKAN1", Color.BLUE));
    }};

}
